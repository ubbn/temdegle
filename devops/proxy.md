# All about proxy

## What is proxy

Aka forward proxy is server that routes all clients' traffic in the network to another network, i.e. internet. It can protects clients and only exposes its own detail, not the clients which actually send requests.

## What is reverse proxy

It is server that routes incoming traffic into a set of servers (web servers) in the traffic, mostly for load balancing, also it protects web servers from outside network, any attack. It doesn't let outside clients know what server actually serves the requests.

## Reverse proxies

**Nginx** is web server that can act as reverse proxy, load balancing, mail proxy, http cache and many more

**HAProxy** is almost defacto load balancer in high loading web servers. It does one thing, one thing the best, load balancing while nginx can do webserver as well as loading balancing.
