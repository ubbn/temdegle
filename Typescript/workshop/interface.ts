enum Color {
    Red,
    Blue,
    Green,
}

interface Car {
    model: string,
    year: number,
    electric: boolean,
    serial: bigint,                 // big integer
    owners: string[]                // array
    inspection: [string, number]    // Tuple
    color: Color
    seller: unknown
    reseller: any
    logo: symbol
}

