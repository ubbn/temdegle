# Concepts

## Types
**Primitive type**: number, string, boolean, bigint, symbol, null, or undefined.  

**Non-primitive type**: object (not commonly used)   
**Other type**: void, never, any, unknown, null

> Their uppercase variants like Number, String, Boolean, Object etc. should not be used  

Use `typeof` to check type of variables
```ts
typeof s === "string"
typeof b === "boolean"
typeof (new Car()) === "object" // not Car, erased at runtime, sorry
```

### Cast
When you are certain that value has specific type, you can assure its type by special keyword `as`
```ts
let someValue: unknown = "this is a string";
let strLength: number = (someValue as string).length;
```


### Type intermix
As long as their properties match, objects of different interfaces or classes can be assigned to each other. It is also called **Structural Type System**
```ts
interface Point {
  x: number;
  y: number;
}

function logPoint(p: Point) {
  console.log(`${p.x}, ${p.y}`);
}

// properties match
const point = { x: 12, y: 26 };
logPoint(point); // logs "12, 26"

// The shape-matching only requires a subset of the object’s fields to match.
const rect = { x: 33, y: 3, width: 30, height: 80 };
logPoint(rect); // logs "33, 3"
```
In other words if the object or class has all the required properties, TypeScript will say they match, regardless of the implementation details.




### Union types
Declares variable that can have multiple types
```ts
let obj: string | string[]
```
Below types can have only those values. It may seem to be similar to `enum`, but it is only for compile time, enum is for runtime, like iterating its value etc.
```ts
type LockStates = "locked" | "unlocked";
type OddNumber = 1 | 3 | 5 | 7 | 9;
const state: LockStates = "locked"
```


### Generics
Array type without generic can contain any types of values, but you can limit it like this:
```ts
type StringArray = Array<string>;
```
Custom type that use generics
```ts
interface Backpack<Type> {
  add: (obj: Type) => void;
  get: () => Type;
}
```



## Interface
It has similar functionalty with `type`, but which is not recommended.  

## Class



## Module




