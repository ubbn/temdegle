



### Shortkeys
Ctrl+.          Quick fix
Shift+Alt+O     Organize imports
Go to Definition F12 - Go to the source code of a symbol definition.
Peek Definition Ctrl+Shift+F10 - Bring up a Peek window that shows the definition of a symbol.
Go to References Shift+F12 - Show all references to a symbol.
Go to Type Definition unassigned - Go to the type that defines a symbol. For an instance of a class, this will reveal the class itself instead of where the instance is defined.
Go to Implementation Ctrl+F12 - Go to the implementations of an interface or abstract method.


Ctrl + Shift + `        Open/hide terminal

