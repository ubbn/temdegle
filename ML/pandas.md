# Pandas

- Built on top of **numpy** module.
- Useful for tabular or structured data while numpy is for more generic numeric operations
- numpy handles arrays while pandas handles matrix or tables
- Unlike numpy, pandas uses index and column data for the table and therefore those 2 eat more memory

## Datatypes

2 data types are provided

### Dataframe

Table represetation

### Series

- Basically one-dimensional list type specific in pandas.
- Can have index with any type, therefore more like dictionary
- Can have a name
- It can form dataframe by becoming its column

```python
import pandas as pd

author = ['Jitender', 'Purnima', 'Arpit', 'Jyoti']
article = [210, 211, 114, 178]

auth_series = pd.Series(author, name="names of authors")
article_series = pd.Series(article)

frame = { 'Author': auth_series, 'Article': article_series }
result = pd.DataFrame(frame)
print(result)

Output:
     Author  Article
0  Jitender      210
1   Purnima      211
2     Arpit      114
3     Jyoti      178
```
