## Setup of Jupyter notebook

1. Installing Octave kernel for jupyter

---

```bash
# install octave on Fedora
dnf install octave

# install octave on RH7
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum localinstall epel-release-latest-7.noarch.rpm
sudo yum install epel-release
sudo yum install octave

# install preqisuite
dnf install redhat-rpm-config
dnf install python-devel

# install actual kernel (root required)
pip install octave_kernel
python -m octave_kernel.install
```

2. Install R for jupyter

---

```bash
# install preqisuite
dnf install R-core
dnf install R-devel
dnf install openssl-devel
dnf install libcurl libcurl-devel

# install R kernel (to executed on R terminal)
install.packages('devtools')
devtools::install_github('IRkernel/IRkernel')
IRkernel::installspec()  # to register the kernel in the current R installation
IRkernel::installspec(name = 'ir33', displayname = 'R 3.3')
```

3. Python

---

```bash
# If you’re running Jupyter on Python 3, set up a Python 2 kernel:
sudo -H pip install --upgrade pip # upgrade pip if required
python2 -m pip install ipykernel
python2 -m ipykernel install --user
```

### Jupyter command

```bash
jupyter-notebook --config=$HOME/.jupyter/jupyter_notebook_config.py
jupyter notebook stop
ipython kernelspec list     # list all kernels for jupyter
jupyter kernelspec list     # list all kernels for jupyter
jupyter kernelspec remove matlab    # remove jupyter kernel


# Notebooks directory
c.NotebookApp.notebook_dir = 'your path'
# IPs can access to the serve
c.NotebookApp.ip = '*'
```

## Setup password for jupyter

```python
from IPython.lib import passwd
password = passwd("Jupyter")
password
'sha1:0c5fb4f17efd:bf1eb31bb3780054759752f3a78f0c1e5298097e'
```

```bash
# Jupyter kernel location
/usr/local/share/jupyter/kernels
/home/ec2-user/.local/share/jupyter/kernels/

----------------
pgrep string # process id
```

## Misc

Commands to check version of each kernel's core from their own command line interface

**For python**

```python
>>> import sys
>>> print(sys.version)
```

**For scala**

```scala
scala> util.Properties.versionString
```

**For julia**

```julia
julia> VERSION
```

**For R**

```R
> version
```

**For octave**

```octave
octave:1> OCTAVE_VERSION()
```

**For bash**

```bash
bash --version
```
