# Conda

Python env and package manager tool

## Package manager

```bash
# list package installed in currently active enivornment
conda list
conda search pandas
conda install pandas
conda remove pandas
```

## Environment manager

```bash
conda env list
conda create --name mfs python=3.6
conda activate mfs
conda deactivate mfs
conda deactivate
```

## Django

```bash
python manage.py runserver --settings=no_debug_settings 0.0.0.0:8000 
./manage.py db
django-admin dbshell
```
