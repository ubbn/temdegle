# Secure communication

The system is only ever as secure as its weakest link.

## TLS

Transport layer security (TLS) is a way for endpoints to communicate securely in network. It encrypts data on transfer and decrypts at reciever side using assymetric encryption which uses private and public keys. This way any non-intended entities cannot intercept or read the data being exchanged over the network. It is implemented in TCP layer so that application layer protocols (HTTP, IMAP, FTP, SMTP) can make use of it. It is most commonly used in public services operating online for a mass of users.

## HTTPS

It is HTTP protocol using TLS certificate for communication.

## mTLS

It is mutual TLS meaning both communicating entities have own certitificates to authorize themselves to one another. In TLS, only server sends its certificate to clients and then clients validate that certificate to be sure that the server is legit and the communiation towards it is secure. While in mutual TLS, addition to this validation, the client also sends back own certificate to server and then server validates if the client is legiticate by sending the certificate CA. Once each entity validates one another's certicate, the communication starts.

Since it requires not only servers, but also clients to have valid certificate, it is not feasible to use it in service operating for public. Instead, it is used in internal networks in organization, enterprise or among a limited number of entities communicating through internet.

## CA

Certificate Authority is entity in the chain that issues a certificate to domain. It generates a pair of public and private key, and stores the private key secretly in itself and sends the public key to the requested entity, or domain owner. Technically, anyone can be CA and issue any certificate to any domain, but the most important thing is whom you can trust. Therefore only handful of CA exist today and its certificates are trusted among public internet. However, many of those trusted CA have been hacked, it is not that silver bullet.
