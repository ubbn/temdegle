# Encoding

Way of transfering or representing raw/binary data in different forms. 
Converting between following encoding formats goes through original binary format. 

## Hexadecimal
Uses 16 characters to interpretate series of 1's and 0's
16 = 2^4 => shrinks data size to 4 times. Hexa means 6, deci means 10

## Base64
Uses 64 characters (english alphabet and some special chars) to interpretate series of 1's and 0's
64 = 2^6 => shrinks data size to 6 times

## ASCII
Uses 128 characters to represent 1 and 0 (only english alphabet)
256 = 2^7 => shrinks raw data size to 7 times

## UTF8
Uses 256 characters to represent 1 and 0 (support multilanguage chars)
256 = 2^8 => shrinks raw data size to 8 times


## Command tools

### Hexadecimal encoding/decoding
```bash
# Encoding
xxd -p <<< "Sample text"
xxd -p <file name to encode> <output encoded file>

# Decoding
echo 48656c6c6f204d616e0a | xxd -p -r
xxd -p -i <encode input file> <decoded output file>
```