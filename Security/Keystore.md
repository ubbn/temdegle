# Java Keytool
It is multipurpose tool to manage Private Keys and Certificates for Java application communicating via HTTPS. It is a container or a password protected encrypted file and each entry is identified by alias. 
 
While ssh-keygen simply and only generates public and private key which can be encrypted with phrase, but private key file is itself not encoded.  

## CLI
```bash
# Generate new RSA keypairi(alias & secret) in new/existing keystore. Also with same command can generate self signing certificate with option -validity <days>
keytool -genkeypair -alias <name> -keyalg RSA -keystore <keystore file name>

# Generate CSR or certificate request, not actual certificate and it  can be sent to CA for signed SSL certificate output csr file is signed by private key identified by alias
keytool -certreq -alias <name> -keystore <ks file> -file <csr file>

# Once CSR is signed by CA and sent as certificate, it can be imported into keystore
keytool -importcert -trustcerts -f <crt file> -alias <name> -keystore <ks file>

# List entries in keystore, -v for verbose mode
keytool -list -keystore <ks file> -storetype <JCEKS|PCKS12>

# Change keystore password
keytool -storepassword -keystore <ks file>

# Rename alias
keytool -changealias -alias <old> -destalias <new> -keystore <ks file>

```


keytool -genkey -alias server-alias -keyalg RSA -keypass changeit -storepass changeit -keystore keystore.jks

keytool -genkeypair -alias "${alias}" -dname "CN=${alias}" -validity 730 -keyalg RSA -keysize 2048 -sigalg SHA256withRSA -storetype ${ewp_storetype} -keystore ${tmp_keystore} -storepass ${password} -keypass ${password}

keytool -import -v -trustcacerts -alias server-alias -file server.cer -keystore cacerts.jks -keypass changeit -storepass changeit
keytool -import -alias foo -file 3.cer -keystore keystore.jks


## Actual
keytool -genkeypair -alias "tmcell_tum_certificate" -dname "CN=EWP_TUM" -validity 8888 -keyalg RSA -keysize 2048 -sigalg MD5withRSA -keystore am-tmcell-tum-keystore -storepass changeit -keypass changeit -storetype JCEKS

kubectl create secret generic am-tmcell-tum-keystore --from-file=am-tmcell-tum-keystore -o yaml --dry-run=client | kubectl apply -f -

# export certificate
keytool -export -keystore am-tmcell-tum-keystore -alias tmcell_tum_certificate -keypass changeit -file tmcel_tum.cer



# Test

# set constant values
export KS=am-tmcell-tum-keystore    # Don't change
export PW=changeit                  # Change to non-default value

# Generate keystore file with self signed certificate (Add more ext parameter value if necessary or required by TUM)
keytool -genkeypair -alias "tmcell_tum_certificate" -validity 8888 -keyalg RSA -keysize 2048 \
-dname "C=MZ,O=TMCELL,L=Default" \
-ext BasicConstraints=ca:true \
-sigalg MD5withRSA -keystore $KS \
-storepass $PW -keypass $PW -storetype JCEKS

# Create kubernetes secret from the keystore file (it must exist before pod starts)
kubectl create secret generic $KS --from-file=$KS -o yaml --dry-run=client | kubectl apply -f -

# Export the certificate from the keystore and send the output cer file to TUM gateway
keytool -export -keystore $KS -alias tmcell_tum_certificate \
 -keypass $PW -storepass $PW -storetype JCEKS -file $KS.cer

# Add keystore password into protstore
lwac-cli -n am-tmcell protstore add --secret=$PW keystore.password



----------
export PW=changeit

# Create a self signed key pair root CA certificate.
keytool -genkeypair -v \
  -alias exampleca \
  -dname "CN=exampleCA, OU=Example Org, O=Example Company, L=San Francisco, ST=California, C=US" \
  -keystore exampleca.jks \
  -keypass:env PW \
  -storepass:env PW \
  -keyalg RSA \
  -keysize 4096 \
  -ext KeyUsage="keyCertSign" \
  -ext BasicConstraints:"critical=ca:true" \
  -validity 9999

# Export the exampleCA public certificate so that it can be used in trust stores..
keytool -export -v \
  -alias exampleca \
  -file exampleca.crt \
  -keypass:env PW \
  -storepass:env PW \
  -keystore exampleca.jks \
  -rfc