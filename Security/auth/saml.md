# SAML

- It is xml based (Security Assertion Markup Language)
- It is an open standard for delegated authentication and authorization unlike OAuth which is only authorization protocol.  
- It is commonly used enterprise SSO while OAuth used for open web SSO.
