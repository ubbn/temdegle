# OAuth

Open Authorization, an open standard for delegated access. Instead of user providing own credentials to 3rd party service, the user grants the permission to identity provider to provide access token (or temporary access key) to the 3rd party and that 3rd party can access to the service provider which uses that identity provider to get the user's information.

![Abstract flow of OAuth](oauth-flow.png)

## History

First started in 2007 by engineers from Twitter, Google and some others and embraced by community and became open standard protocol, OAuth 1.0 in 2009. In 2013, OAuth 2.0 was released which got attacked by many and soon after OAuth 2.1 was released.

## Uses

OAuth is authorization protocol rather than authentication protocol while OpenId is authentication protocol. And both can be used for more secure access control.

Benefits:

- User credentials won't be shared to 3rd party
- User doesn't need to create new accounts in 3rd party
- Also known as Three-legged Auth (resource owner, authorization server and resource server vs 3rd party)

### OpenId Connect

It is more complete solution which is based on OAuth 2.0 and add authentication protocol.
