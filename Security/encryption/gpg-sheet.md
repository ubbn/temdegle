# GPG

Gnu Privacy Guard, open source CLI tool for encryption and signing data with keys

## Encrypt symmetric, generates ascii file with extension .asc 
gpg -a --symmetric --cipher-algo AES256 <file to encrypt>

## Decrypt
gpg -a --decrypt <encrypted>.asc --output <decrypted output>
