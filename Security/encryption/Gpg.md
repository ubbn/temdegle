# Gnu Public Guard (GPG)

It is a cryptographic tool for securing data in GNU system.

## Command line
```bash
# Encrypt symmetric, generates ascii file with extension .asc 
gpg -a --symmetric --cipher-algo AES256 <file to encrypt>

# Decrypt
gpg -a --output <decrypted> --decrypt <encrypted>.asc

# Password is cached for sometime, to clear cache
echo RELOADAGENT | gpg-connect-agent

# encrypt files
gpg -c --no-symkey-cache file.txt

# decrypt files
gpg --no-symkey-cache file.txt.gpg

```


```
# Enable/disable networking completely
nmcli networking off/on

#Just for WiFi
nmcli radio wifi off/on
```