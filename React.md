# Concepts in React

## Rendering

One component can render only one parent DOM node, not siblings.  
Each function and variables are re-created between rerenders of single component.  

render() - the heart of any React component which describes what the component is supposed to look like. React will call render() on initial mount and on every state change. 
setState() - brains of React component which tells React we are changing state thereby triggering re-render of component and its children. 
componentDidMount() - special lifecycle method called after a component is mounted on DOM. 
componentDidUpdate() - called everytime the component is re-rendered. 

## React Hooks

Hooks are functions hooked into specific lifecycle of React components. There are few simple rules about hooks:

- Don't call hooks in loop, nested function or before condition
- Call only from functional components

## useRef

- It allows to persist value between rerenders
- Also enables access to DOM element directly

### useContext

Allows to subscribe to React context without passing props heriarchially  

```js
const locale = useContext(LocaleContext);
```

### useCalback

It memoizes callback functions and returns memoized function reference which will remain same between rerenders.

```js
const onClick = useCallback((event) => {
  // do something about the event
}, [deps])
```

### useMemo

It memoizes expensive function and don't run the function on every rerender of the the component. 

```js
const memodValue = useMemo(() => {
  // do expensive calculation and return value
  return somePreciousValue
}, [deps])
```

