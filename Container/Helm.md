# Helm
Application package manager for Kubernetes (like apt, yum ...)
Way to describe/share/replicate application with required K8s resources 

## Concepts
**Chart** Helm chart is compressed archive file `*.tgz` containing yaml files and `Dockerfile`. `Chart.yaml` required for chart. version is for chart and appvesion for application included in the chart     

**Release** Application running instance deployed from the chart  

**Repository** Http server where helm charts are stored/distributed 

**Dependencies** Chart can depend on other charts which are defined in `requirement.yaml` 

**Templates** yaml files stored in dir `/templates`. Files have variables inside whose default values are provided by `values.yaml` file.   
**Hooks** Execution of logic during certain stage of deploying application   

## Command line
**helm** is client side executable. **thriller** is executable binary in backend side. 
```bash
# Search from helm repository
helm search
# Pulls char from repository and deploys local k8s, creates A RELEASE
helm install stable/mariadb
# Just fetch chart
helm fetch stable/mariadb

# List all running releases, check status and get details of running RELEASE
helm list --all
helm status RELEASE_NAME
helm get RELEASE_NAME 

# Local helm repository
ll $HOME/.helm/repository/

# Remove application deployment from k8s
helm delete [flags] RELEASE_NAME --purge

# 
```




