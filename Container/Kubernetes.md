# Kubernetes

System that orchestrates containerized applications by automating scale, deployment, test. Basically it manages infrastructure resources (network, volume, secure keys ...) required for your application environment and make their desired status available with just declarative syntaxes.

## 1. Concepts

**One Container One Process** principle

etcd - runs on master, stores kv pair, handles states you wanted  
kube-apiserver, kube-scheduler - run on master  
kubelet - agent runs on every node  
kube-proxy - agent runs on every node which handles implementing virtual IP for Services, enables communication between Pods

**Node** can have many pods. And It must have kubelet agent and container runtime (Docker, rkt etc.) to be used.

**Pod** can have many containers, Pod is smallest unit of resource in K8s. Instance of container, can be killed/restarted/stopped/failed/created... Pod alone is for only dev or one-off use cases

**Deployment** can have many Pods and manages them. Abstraction that handles controlling Pods which are alone nothing, just zombie container instance.

**Namespace** - Virtual cluster in same physical cluster.

**Service** abtraction of exposing deployment(set of pods) to outside. Like IP address, port, DNS that are static and non-changing without depending on backend, like how many pods running and how they are running. It is defined in YAML file and fed to API-server in master node. It is basically IP address giver to expose application to its clients.  
Network type ClusterIP - only accessible in cluster internally

**Ingress** allows external traffic outside of cluster to Services (pods). It has set of rules which requests to be handles by which Services.

**Job** -

**Volumes** - Keeps persistent state, independent and outside of pod which is ephemeral. empty-dir volume is default so ephemeral. nfs volume is external mount point. Persistent Volume(PV) - specification about actual storage underneath (nfs, aws, google, azure ...). Persistent Volume Claim (PVC) - exposes PV to pods which doesn't know where actually it stores data.  
`kubectl get pv # list all pvs`  
`kubectl get cs # list all storage class`

**ConfigMap** - abtraction to decouple configurations from container. It stores configuration (non-sensitive unlike Secrets & unencrypted data) in key value pair.

**Secrets** - K8s object that manages sensitive information, such as passwords, OAuth tokens, and ssh keys.

**Persistent Storage** - accessMode ReadWriteOnce is for read/write for 1 node only. ReadWriteMany - accessible by many nodes

## 2. Command Line Interface

First start orchestrator engine to use `kubectl`

```b
minikube start
minikube dashboard
```

## 3. Kubectl

Agent service on node that communicates with master node

```bash
# Checking Configuration
kubectl config view/set/unset                  # view/set kubeconfig file ${HOME}/.kube/config
kubectl config get-contexts

# Adding resources
kubectl apply -f <yaml file|dir|url>           # create resource(s) from different sources
kubectl create                                 # create resource
kubectl create deployment nginx --image=nginx  # start a single instance of nginx
kubectl explain pods                           # get the documentation for pod and services manifests

# Viewing finding resources
kubectl get services/svc -A              # List all services in all namespace
kubectl get svc --show-labels            # List all services and their labels
kubectl get nodes/no
kubectl get pods -A                      # List all pods in all namespaces
kubectl get pods -o wide                 # List all pods in the namespace, with more details
kubectl get pod my-pod -o yaml           # Get a pod's YAML
kubectl get pod my-pod -o yaml --export  # Get a pod's YAML
kubectl get deployment/deploy -A         # List all deployments
kubectl get deployment my-dep            # List a particular deployment
kubectl get namespace/ns
kubectl get psp                          # List Pod Security Policy (cluster wide resource)
kubectl get sa                           # List service accounts
kubectl get events                       # List latest events

kubectl get cm                          # List config maps
kubectl get cm <cm name> -o yaml        # List one config maps's all properties
kubectl edit cm <cm name>               # Edit config map
kubectl get pv                          # List persistent volume
kubectl get pvc                         # List persistent volume claim
kubectl get all                         # List all resources

# List all container in a pod
kubectl get pods <pod name> -o jsonpath='{.spec.containers[*].name}'
# List all container images in a pod
kubectl get pods -o jsonpath="{..image}" |tr -s '[[:space:]]' '\n' |sort |uniq -c
# Connect to single container pod
kubectl exec -it <pod name> -- /bin/bash
kubectl exec <pod name> cat /proc/1/mounts
# Connect to multi container pod
kubectl exec -it <pod name> --container <container name> -- /bin/bash

kubectl api-resources               # shows all API resource in the cluster
```

### Creating POD

```bash
# Creates new deployment with a new Pod from cli
kubectl create deployment <label> --image=<image name>:<tag>
# Creates new Pod from YAML file
kubectl create -f <path to yaml file>
# Creates new deployment from cli
kubectl run <> -f <path to yaml file>
# Exposes deployment by creating new service
kubectl expose deployment <label> --type=LoadBalancer --port=8080
# Get pods, services
kubectl get pod,svc -n kube-system
```

### Managing Pod

```bash
# Copy file between local and pod
kubectl cp <local_filepath>/<filename> <namespace>/<pod>:<pod_filepath>
kubectl cp <pod>:/path/in/pod.file /local/file/system
```

### Cleanup

```bash
kubectl delete service hello-node
kubectl delete deployment hello-node
minikube stop                               # Stops minikube VM
minikube delete                             # Deletes minikube VM
```

### Troubleshooting with kubectl

```bash
kubectl cluster-info            # Shows status of cluster
kubectl cluster-info dump       # Shows status of cluster in debug detail
kubectl get componentstatuses

kubectl get                     # list resources
kubectl get pods -w             # status of pods in live
kubectl describe                # show detailed information about a resource
kubectl logs --tail=20 <pod name>  # print the logs from a container in a pod
kubectl logs <pod name> -c <container name> # Incase of multi-container pod, specify which ones to look
kubectl exec                    # execute a command on a container in a pod`
```

### Configmap manipulation

Export configmap into file

`kubectl get cm <configmap name> -o jsonpath='{.data.somekey-file-name\.ini}' > somekey-file-name.ini`

Create configmap from content of given directoy. Each file in directory will be key and its content will be value

```bash
kubectl create cm <name of configmap> \
 --from-file=<directory path | file path> \
 -o yaml --dry-run=client | kubectl apply -f -
```

### Create secret from password set as environment variable

```bash
kubectl create secret generic <name of the secret>\
 --from-literal=mypasswd=$(echo -n $MYPASS | base64)
```
