# Docker

[**Docker**](https://docs.docker.com/): OS-level virtualization software container platform which automates deployment of application inside sandbox/container software. Container is not whole virtual OS, it is only required libraries/binaries to run the software in isolated environment. Benefits:     
- any app, dev stack or environment 
- lightweight, uses less resource from host
- automates deployment, env. setup and configuration, test work
- environment/os independent: app will work same regardless of environment/os

## 1. Concepts

**Image** - a file comprise of multiple layers, dedicated to run on docker engine. Each layer is viewable via cli `docker image history <image name>:<tag>`   It contains file system, all dependencies, libraries, binaries, code etc.

**Container** - an instance of the image or a running process to keep environment isolated from others   

**Docker daemon** - Docker uses client server architecture. Docker daemon `dockerd` acts as server and CLI is client. a docker daemon manages images, containers and listens to its client via REST API. 

**Registeries** - Repository where docker images are stored/shared out. By default it is Docker Hub.  

## 2. Command Line
For complete offical reference of docker CLI, refer to [here](https://docs.docker.com/reference/)

### 2.1. Check status 
```bash
# Start docker daemon manually if not started already
systemctl status docker 
systemctl start docker

# List all available local images  
docker images

# Show changes made to image
docker image history <name>:<tag>

# Get new image from docker hub/repository  
docker pull busybox

# List running containers 
docker ps
# List all of history
docker ps -a 
```
### 2.2. Run container
Running container is ephemeral process, it will just die after run. To run as deamon, you should run it in **detached** mode with `-d` flag
```bash
# Run container from image  
docker run <image name>

# Run new container execute command and will die 
docker run <image name> echo "hello from tty"

# Run interactive shell to the container
docker run -it <image name> /bin/sh
# Run new container in deamon and execute commands when started
docker run -d <image name> /bin/sh -c "apt update && apt -y upgrade"

# Run detached mode(-d) and publishing port(-P)
docker run -d -p <host port>:<c.port> --name <container name> <image name>:<tag name>
```
### 2.3. Manage container

```bash
# List containers same as docker ps
docker container list -a
# Start stopped container interactively and attach STDIN/OUT
docker container start <con.name or CID> -ia 
# Stop detached container
docker container stop <con.name or CID>
# or just   
docker stop <CID>
# Displays running processes on container 
docker container top <con.name or CID> 
# Show logs from running container
docker container logs <CID>
# Live usage status of all running containers
docker container stats
# Display info about container
docker container inspect <CID>
```

### 2.4. Interact with container

```bash
# Get shell access to running container
docker exec -it <CID> bash
# Attach local STDIN/OUT to running container, possible to set detach-keys
docker attach <CID>
# Copy files from host to container
docker cp <source path> <CID>:/destination-path 
# Mount directory from host to container
docker run -v <path in host>:<path in container> <image name>
# Create image from container
docker commit -m "optional message" <CID> <image name>:tag
```

### 2.5. Clean up containers/images
First containers and their all history must be deleted in order to delete an image  
```bash
# Remove one container, it must be stopped before
docker rm <CID>    
# Don't truncate container id, all and quite mode (show only container id)
docker rm `docker ps --no-trunc -aq` 
# Run container without leaving history
docker run -rm <image name>
# Then the image can be removed, the 2nd removes all untaged images
docker rmi <image name> 
docker images -q --filter "dangling=true" | xargs docker rmi
```

## 3. Dockerfile
**`dockerfile`** - simple text file containing a list of commands which docker client will execute while creating image. In other words, it is automation script to create image. This is what makes docker very robust meaning that it is repetitive and way to make application deployment portable & automate.  
```bash
# Build image from dockerfile
docker build --tag <name>:<tag> <dir where Dockerfile exists>
# Query back images created by iamge or/and tag names
docker images --filter=reference=<image name>:<tag name>
```
Each instruction in the file adds new layer on top of base layer of the image. 

