# Tmux
Terminal multiplixer
Key binding `Ctrl+b`

## 1. Window navigation
Below are some most common commands for managing Tmux windows and panes:  

### 1.1. Create
`K %` Split current pane horizontally into two panes  
`K "` Split current pane vertically into two panes  
`K c` Create a new window (with shell)  

### 1.2. Navigate
`K ;` Toggle between the current and previous pane  
`K o` Go to the next pane  
`K 0` Switch to window 0 (by number )  

### 1.3. Manage open window
`K w` Choose window from a list  
`K ,` Rename the current window  
`K z` Make current pane FULL  
`K x` Close the current pane  

## 2. Session management
`tmux new -s my_session` 			- start new named session  
`Ctrl-b + d`						- Detach from the session.  
`tmux ls`							- list sessions  
`tmux attach-session -t my_session` - Re-attach to detached session  

## 3. Mouse
- Press `Shift` and highlight with mouse to copy to clipboard