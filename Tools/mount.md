# Quick notes for mounting media

## List external drives

sudo fdisk -l

## Create destination for mount

sudo mkdir /media/${USER}/dest

## Mount the device on dist dir

sudo mount -o uid=1000,gid=1000 /dev/sda1 /media/${USER}/dest

## Make mount writeable

sudo mount -o remount,rw /dev/sda1

## Umount the device

sudo umount /dev/sda1
