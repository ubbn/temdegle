# VScode

It is wonderful editor

## Shortkeys

Keybindings are not workspace specific. They are configured in user level, $HOME/.config/Code/

### Editor navigation

Ctrl+,                Open settings
Ctrl+K,S              Open keybindings
Ctrl+P                Open recent files
Ctrl+R                Open recent projects
Ctrl+Shft+P           Open command platte
Ctrl+Alt+Right/Left   Move open file to right/left side of split view

### Code navigation

F12                   Go to definition
Shft+F12              Go to usages
Ctrl+Shft+.           Open map of the open file
Ctrl+Shft+O           Open map of the open file
Ctrl+Shft+X           Extensions
Ctrl+P,#              Search symbol (constant, method, class ...) in entire editor
Ctrl+Shft+Space       Show function doc in popup, and up/down for surf

### Code manipulation

Ctrl+L                Highlight current line
Shft+Alt+Right        Expand selection to block by block
Shft+Alt+O            Clean up unused imports
Shft+Ctrl+I           Format code

### My custom keybindings

Alt+1             File exporer
Alt+2             Git explorer default
Alt+3             GitLens explorer
Alt+-             Collapse folders in file explorer
Ctrl+D            Compare selected files
Ctrl+Ö,H          Open file history
Ctrl+Ö,D          Open changes
Ctrl+Shft+G       Git blame gutter
Ctrl+Shft+-/+     Fold/unfold current block
Ctrl+K, J         Unfold all blocks

## Plugins/Extensions

List all installed

```bash
code --list-extensions
code --install-extension <extension-id>
code --uninstall-extension <extension-id>
# Install all listed extensions
code --list-extensions | xargs -L 1 echo code --install-extension
```

- Installed  extensions are shown below by 2022.05.14
amazonwebservices.aws-toolkit-vscode
dbaeumer.vscode-eslint
dsznajder.es7-react-js-snippets
eamodio.gitlens
esbenp.prettier-vscode
formulahendry.code-runner
ms-azuretools.vscode-docker
ms-python.python
ms-python.vscode-pylance
styled-components.vscode-styled-components
VisualStudioExptTeam.vscodeintellicode
vscjava.vscode-java-dependency
vscode-icons-team.vscode-icons

### Live templates / Snippets

Feature to add boilplate template code from few shortcuts. Search plugins with category: `@category:"snippets"`


