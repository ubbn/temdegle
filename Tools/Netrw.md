# Netrw

Vim's default file explorer

## Director navivation

u - switch to previously visited dir
U - reverse to u
Ctrl+w - switch between open buffers

## Switch to dir on close

Switch to desired directory, run following keys to exit vim yet remain on current dir  
`c :shell`

% - create new file
d - create new directory
o - opens file horizontal split view
v - opens file verical/bosoo split view
p - preview file

## Mark

mf - Toggle whether the file/directory is marked
mt - Mark the directory under the cursor as the copy target
mc - Execute the copy operation
mm - Execute the move operation
mu - Unmark all marked items
mx - apply shell command on marked files

## Bookmkark

mb - bookmark current dir
qb - list bookmarks and history
gb - to previous bookmark
4gb - to 4rd bookmarked dir
